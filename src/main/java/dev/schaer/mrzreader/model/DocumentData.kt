package dev.schaer.mrzreader.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.time.format.DateTimeFormatter

private val ID_DATE_FMT = DateTimeFormatter.ofPattern("yyMMdd")

@Parcelize
data class DocumentData(val documentNumber: String, val expiryDate: LocalDate, val dateOfBirth: LocalDate):
    Parcelable {
        constructor(documentNumber: String, expiryDate: String, dateOfBirth: String):
                this(documentNumber.replace("<", ""), parseDate(expiryDate), parseDOB(dateOfBirth))

        companion object {
            const val ID = "docData"

            private fun parseDOB(text: String): LocalDate {
                val date = parseDate(text)
                if(date.isBefore(LocalDate.now())) {
                    return date;
                }
                // only the two least significant digits are given
                return date.minusYears(100)
            }

            private fun parseDate(text: String): LocalDate {
                return LocalDate.parse(text, ID_DATE_FMT);
            }
        }

        fun isValid(): Boolean {
            val today = LocalDate.now();
            return dateOfBirth.isBefore(today) //
                    && expiryDate.isAfter(today) //
                    && expiryDate.isBefore(today.plusYears(10))
        }
    }
