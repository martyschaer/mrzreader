package dev.schaer.mrzreader.utils

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class ErrorDialog : DialogFragment() {
    companion object {
        private const val ARG_MSG = "message"

        fun newInstance(msg: String): ErrorDialog {
            val dialog = ErrorDialog()
            val args = Bundle()
            args.putString(ARG_MSG, msg)
            dialog.arguments = args
            return dialog
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context)
            .setMessage(arguments?.getString(ARG_MSG))
            .setPositiveButton(android.R.string.ok) { _, _ -> activity?.finish() }
            .create()
    }
}