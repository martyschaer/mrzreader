package dev.schaer.mrzreader.ocr;

import android.util.Log;

import dev.schaer.mrzreader.model.DocumentData;

public class Mrz {

  private static final String TAG = "Mrz";

  private static final int[] PASSPORT_DOCNO_INDICES = new int[]{0, 9};
  private static final int[] PASSPORT_DOB_INDICES = new int[]{13, 19};
  private static final int[] PASSPORT_EXP_INDICES = new int[]{21, 27};

  private static final int[] ID_DOCNO_INDICES = new int[]{5, 14};
  private static final int[] ID_DOB_INDICES = new int[]{0, 6};
  private static final int[] ID_EXP_INDICES = new int[]{8, 14};

  private String mrz;

  public Mrz(String mrz) {
    this.mrz = mrz;
    cleanMRZString();
  }

  /**
   * Does some basic cleaning on the MRZ string of this object
   * Done before checksum verification so may throw errors, these are ignored
   */
  private void cleanMRZString() {
    try {
      mrz = mrz.replace(" ", ""); // Delete any space characters
      mrz = mrz.replace("\n\n", "\n"); // replace double newline with single newline
      mrz = mrz.replace('O', '0'); // replace any O with 0, because O is most likely a false reading
      String[] spl = mrz.split("\n");
      mrz = spl[0] + "\n" + spl[1]; // Extract only first 2 lines, sometimes random errorous data is detected beyond.
    } catch (Exception ignored) {
    }
  }

  /**
   * Performs checksum calculation of the given string's chars from start til end.
   * Uses value at index {@code checkIndex} in {@code string} as check value.
   *
   * @param string     String to be checked
   * @param ranges     indices of substrings to be checked
   * @param checkIndex index of char to check against
   * @return boolean whether check was successful
   */
  private static boolean checkSum(String string, int[][] ranges, int checkIndex) {
    int[] code = {7, 3, 1};
    int checkValue = Character.getNumericValue(string.charAt(checkIndex));
    int count = 0;
    float checkSum = 0;
    for (int[] range : ranges) {
      char[] line = string.substring(range[0], range[1]).toCharArray();
      for (char c : line) {
        c = c == 'O' ? '0' : c;
        int num;
        if (Character.toString(c).matches("[A-Z]")) {
          num = ((int) c - 55);
        } else if (Character.toString(c).matches("\\d")) {
          num = Character.getNumericValue(c);
        } else if (Character.toString(c).matches("<")) {
          num = 0;
        } else {
          return false; //illegal character
        }
        checkSum += num * code[count % 3];
        count++;
      }
    }
    int rem = (int) checkSum % 10;
    return rem == checkValue;
  }

  /**
   * Returns relevant data from the MRZ in a DocumentData object.
   *
   * @return DocumentData object
   */
  public DocumentData getPrettyData() {
    if (mrz.startsWith("P")) {
      String documentNumber = mrz.split("\n")[1].substring(PASSPORT_DOCNO_INDICES[0], PASSPORT_DOCNO_INDICES[1]);
      String dateOfBirth = mrz.split("\n")[1].substring(PASSPORT_DOB_INDICES[0], PASSPORT_DOB_INDICES[1]);
      String dateOfExpiry = mrz.split("\n")[1].substring(PASSPORT_EXP_INDICES[0], PASSPORT_EXP_INDICES[1]);
      return new DocumentData(documentNumber, dateOfExpiry, dateOfBirth);
    } else if (mrz.startsWith("I")) {
      String documentNumber = mrz.split("\n")[0].substring(ID_DOCNO_INDICES[0], ID_DOCNO_INDICES[1]);
      String dateOfBirth = mrz.split("\n")[1].substring(ID_DOB_INDICES[0], ID_DOB_INDICES[1]);
      String dateOfExpiry = mrz.split("\n")[1].substring(ID_EXP_INDICES[0], ID_EXP_INDICES[1]);
      return new DocumentData(documentNumber, dateOfExpiry, dateOfBirth);
    }
    return null;
  }

  /**
   * Checks if this MRZ data is valid
   *
   * @return boolean whether the given input is a correct MRZ.
   */
  public boolean valid() {
    try {
      Log.i(TAG, "MRZ starts with " + mrz.substring(0, 1));
      if (mrz.startsWith("P")) {
        return checkPassportMRZ(mrz);
      } else if (mrz.startsWith("I")) {
        return checkIDMRZ(mrz);
      }
    } catch (Exception ignored) {
      // Probably an outOfBounds indicating the format was incorrect
    }
    return false;
  }

  private boolean checkIDMRZ(String mrz) {
    boolean firstCheck = checkSum(mrz.split("\n")[0], new int[][]{ID_DOCNO_INDICES}, 14); //Checks document number
    Log.i(TAG, "[ID] Document Number OK? " + firstCheck);
    boolean secondCheck = checkSum(mrz.split("\n")[1], new int[][]{ID_DOB_INDICES}, 6); //Checks DoB
    Log.i(TAG, "[ID] DoB OK? " + secondCheck);
    boolean thirdCheck = checkSum(mrz.split("\n")[1], new int[][]{ID_EXP_INDICES}, 14); //Checks expiration date
    Log.i(TAG, "[ID] DoE OK? " + thirdCheck);
    return firstCheck && secondCheck && thirdCheck;
  }

  private boolean checkPassportMRZ(String mrz) {
    boolean firstCheck = checkSum(mrz.split("\n")[1], new int[][]{PASSPORT_DOCNO_INDICES}, 9); // Checks document number
    Log.i(TAG, "[Passport] Document Number OK? " + firstCheck);
    boolean secondCheck = checkSum(mrz.split("\n")[1], new int[][]{PASSPORT_DOB_INDICES}, 19);
    Log.i(TAG, "[Passport] DoB OK? " + secondCheck);
    boolean thirdCheck = checkSum(mrz.split("\n")[1], new int[][]{PASSPORT_EXP_INDICES}, 27);
    Log.i(TAG, "[Passport] DoE OK? " + thirdCheck);
    return firstCheck && secondCheck && thirdCheck;
  }

  public String getText() {
    return mrz;
  }
}
