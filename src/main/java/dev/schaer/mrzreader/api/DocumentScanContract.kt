package dev.schaer.mrzreader.api

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import dev.schaer.mrzreader.camera.CameraActivity
import dev.schaer.mrzreader.model.DocumentData

class DocumentScanContract: ActivityResultContract<Unit, DocumentData>() {
    override fun parseResult(resultCode: Int, data: Intent?): DocumentData? {
        return when (resultCode) {
            Activity.RESULT_OK -> data?.getParcelableExtra(DocumentData.ID)
            else -> null
        }
    }

    override fun createIntent(context: Context, input: Unit?): Intent {
        val intent = Intent(context, CameraActivity::class.java)
        return intent
    }
}