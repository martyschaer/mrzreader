# MRZ Reader
An android library to read machine readable zones
(on machine readable travel documents).
Based on the efforts of
[Delft University of Technology](https://www.tudelft.nl/)
in the
[polling-station-app](https://github.com/digital-voting-pass/polling-station-app).
